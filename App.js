/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Image, ScrollView, AsyncStorage } from 'react-native';
import Colors from './src/constants/Colors';
import ApiServices from './src/constants/ApiServices';
import ModalAddRepository from './src/components/ModalAddRepository';
import ModalLoading from './src/components/ModalLoading';
import Repository from './src/components/Repository';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {

  state = {
    repositories: [],
    count_repos: 'Galeria vazia',
    isModalAddVisible: false,
    isLoading: false,
  };

  async componentDidMount() {
    var repositories = JSON.parse(await AsyncStorage.getItem("@GitHubApi:repositories")) || [];

    this.setState({
      repositories,
      count_repos: (repositories.length === 0) ? 'Galeria vazia' : repositories.length + ' repositório(s)',
    });
  }
  
  addRepository = async (owner, repo) => {

    this.setState({
      isModalAddVisible: false,
      isLoading: true,
    });

    const URI = ApiServices.getRepository + owner + '/' + repo;

    try {
      let response = await fetch(URI);

      if (response.ok) {

        let responseJson = await response.json();

        var arr = [...this.state.repositories];
        arr.unshift(responseJson);

        this.setState({
          isModalAddVisible: false,
          isLoading: false,
          count_repos: arr.length + ' repositório(s)',
          repositories: arr,
        });

        await AsyncStorage.setItem("@GitHubApi:repositories", JSON.stringify([...this.state.repositories, responseJson]));

      } else {
        this.setState({ isLoading: false});
        alert('Repositório não encontrado.');
      };

    } catch (error) {
      this.setState({ isLoading: false});
      alert('Falha ao adicionar repositório. Tente novamente.');
    }
  }

  deleteRepository = async (key) => {
    this.state.repositories.splice(key, 1);

    this.setState({
      repositories: this.state.repositories,
      count_repos: (this.state.repositories.length === 0) ? 'Galeria vazia' : this.state.repositories.length + ' repositório(s)',
    });
    await AsyncStorage.setItem("@GitHubApi:repositories", JSON.stringify([...this.state.repositories]));
  }

  render() {

    repositories = this.state.repositories.map((val, key) => {
      return <Repository key={key} keyval={key} val={val} onDelete={ ()=> {this.deleteRepository(key)} } />
    });

    return (
      <View style={styles.container}>

          <View style={styles.header}>

            <Image 
              style={styles.headerLogo}
              source={require('./src/assets/images/github-logo.png')} />
            
            <Text style={styles.headerTitle}>GALERIA DE REPOSITÓRIOS</Text>

            <TouchableOpacity style={{margin: 20}} onPress={ ()=> {this.setState({isModalAddVisible: true})} }>
                <Text style={styles.headerBtnAddText}>+</Text>
            </TouchableOpacity>
          
          </View>

          <View style={styles.contentBody}>

            <ScrollView style={{ marginBottom: 10}}>
              <Text style={styles.countReposText}>{this.state.count_repos}</Text>

              {repositories}

            </ScrollView>

          </View>
          
          <View style={styles.footer}>
            <TouchableOpacity  style={styles.floatingButtonAdd} onPress={ ()=> {this.setState({isModalAddVisible: true})} }>
                <Text style={styles.headerBtnAddText}>+</Text>
            </TouchableOpacity>
          </View>

          <ModalAddRepository 
            modalVisible={this.state.isModalAddVisible}
            onAdd={ this.addRepository }
            onClose={ ()=> {this.setState({isModalAddVisible: false})}} />

          <ModalLoading modalVisible={this.state.isLoading} />
          
      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  header: {
    backgroundColor: Colors.headerBackgroundColor,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 50,
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 5,
    marginRight: 5
  },
  headerLogo: {
    width: 20,
    height: 20,
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 20
  },
  headerTitle: {
    textAlign: 'left',
    color: Colors.textColor,
    fontSize: 14,
    padding: 15,
    marginTop: 20,
    marginBottom: 20,
    fontWeight: 'bold',
    fontFamily: 'Roboto',
    width: 250
  },
  headerBtnAddText: {
    color: Colors.textColor,
    fontSize: 35,
    fontFamily: 'serif',
    fontWeight: 'normal',
  },
  contentBody: {
    flex: 1,
    backgroundColor: Colors.contentBodyColor,
  },
  countReposText: {
    color: '#BBBBBB',
    margin: 10,
    fontSize: 12,
    fontWeight: 'normal',
  },
  footer: {
    position: 'absolute',
    alignItems: 'center',
    bottom: 0,
    left: 0,
    right: 0,
  },
  floatingButtonAdd: {
    backgroundColor: Colors.floatBtnColor,
    width: 55,
    height: 55,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 10,
    zIndex: 10,
    marginBottom: 80,
    marginLeft: 250,
  },
});
