const baseUrl = 'https://api.github.com';

export default {
    baseUrl,
    getRepository: baseUrl + '/repos/',
    getOwner: baseUrl + '/users/',
};