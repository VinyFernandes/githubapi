import React, { Component } from 'react';
import { StyleSheet, View, Text, Modal, TouchableOpacity } from 'react-native'

export default class ModalLoading extends React.Component {

    render(){
        return(
            <Modal visible={this.props.modalVisible} animationTYpe="fade" transparent={true} onRequestClose={ ()=> {this.props.onClose}}>
                <View style={styles.container}>

                    <View style={styles.innerContainer}>

                        <Text style={styles.text}>Aguarde um momento...</Text>

                    </View>

                </View>
            </Modal>
        );
    };
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: 100,
        padding: 20,
    },
    innerContainer: {
        margin: 10,
        paddingTop: 50,
        paddingBottom: 50,
        paddingLeft: 50,
        paddingRight: 50,
        borderRadius: 1,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#a09d99',
        borderRightWidth: 3,
        borderRightColor: '#a09d99',
    },
    text: {
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
        padding: 10,
        fontFamily: 'Roboto',
        fontWeight: 'normal',
    }
});