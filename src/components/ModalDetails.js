import React from 'react';
import { StyleSheet, View, Text, Modal, TouchableOpacity, Image } from 'react-native';
import Colors from '../constants/Colors';

export default class ModalDetails extends React.Component {

    render(){

        return(
            <Modal visible={this.props.modalVisible} animationTYpe="fade" transparent={true} onRequestClose={ this.props.onClose }>
                <View style={styles.container}>

                    <View style={styles.header}>
                        <Text style={styles.modalTitle}>DETALHES</Text>
                    </View>

                    <View style={styles.innerContainer}>

                        <Image
                            style={styles.avatar}
                            source={{uri: this.props.owner.avatar_url}} />

                        <Text style={{fontWeight: 'bold', color: Colors.textColorDefault}}>{this.props.owner.name}</Text>

                        <Text>{this.props.owner.location}</Text>

                        <Text style={{fontWeight: 'bold'}}>Owner: <Text style={{fontStyle: 'italic'}}>{this.props.owner.login}</Text></Text>

                        <Text><Text style={{fontWeight: 'bold'}}>Id: </Text>#{this.props.owner.id}</Text>

                        <View style={styles.contentRepo}>
                            <Text style={[styles.titleContentRepo, {color: Colors.textColorDefault}]}>REPOSITÓRIO</Text>
                            <Text><Text style={{alignSelf: 'flex-start', fontWeight: 'bold'}}>Id: </Text>#{this.props.repository.id}</Text>
                            <Text><Text style={{alignSelf: 'flex-start', fontWeight: 'bold'}}>Nome:  </Text>{this.props.repository.name}</Text>
                            <Text><Text style={{fontWeight: 'bold'}}>Descrição: </Text>{this.props.repository.description}</Text>
                        </View>

                        <View style={styles.btnContainer}>
                            <TouchableOpacity onPress={ this.props.onClose } style={[styles.btn, styles.btnDark]}>
                                <Text style={styles.btnText}>Fechar</Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                </View>
            </Modal>
        );
    };
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        padding: 20,
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 75,
        marginBottom: -15,
    },
    modalTitle: {
        margin: 10,
        marginLeft: 0,
        textAlign: 'left',
        color: Colors.textColor,
        fontSize: 14,
        padding: 15,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        width: 250,
    },
    innerContainer: {
        padding: 10,
        borderRadius: 1,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
    },
    avatar: {
        borderWidth: 2,
        borderColor: Colors.backgroundColor,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 100,
        padding: 30,
    },
    contentRepo: {
        alignSelf: 'flex-start',
        margin: 20,
    },
    titleContentRepo: {
        fontWeight: 'bold',
        paddingTop: 10, 
        paddingBottom: 10, 
        borderBottomWidth: 1, 
        borderBottomColor: Colors.dividerColor,
    },
    btnContainer: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    btn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
        borderBottomWidth: 3,
        borderBottomColor: Colors.btnBorderColor,
        borderRightWidth: 3,
        borderRightColor: '#c8c5c0',
        margin: 10,
        padding: 16,
    },
    btnText: {
        color: Colors.textColor,
        fontFamily: 'Roboto',
        fontWeight: 'normal',
        fontSize: 14,
    },
    btnDark: {
        backgroundColor: Colors.btnDark,
    },
});
